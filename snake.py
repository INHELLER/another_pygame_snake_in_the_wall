#!/usr/bin/env python3

import pygame
import sys
import random
import collections


def in_bounds(pos, min_pos, max_pos):
    return min_pos[0] <= pos[0] <= max_pos[0] and min_pos[1] <= pos[1] <= max_pos[1]


def get_invalid_direction(in_snake_head, in_snake_body):
    return in_snake_body[0] - in_snake_head[0], in_snake_body[1] - in_snake_head[1]


def draw_bounds(in_display, display_size, block_size, bound_color):
    rectangles = (
        (0, 0, block_size, display_size[1]),
        (0, 0, display_size[0], block_size),
        (0, display_size[1] - block_size, display_size[0], block_size),
        (display_size[0] - block_size, 0, block_size, display_size[1])
    )
    for rectangle in rectangles:
        pygame.draw.rect(in_display, bound_color, rectangle)


def generate_snake():
    head_block = (
        BLOCKS_WIDTH // 2,
        BLOCKS_HEIGHT // 2,
    )
    local_snake_head = (head_block[0] * BLOCK_SIZE, head_block[1] * BLOCK_SIZE)
    local_snake_body = collections.deque()
    for i in range(1, 4):
        local_snake_body.append((head_block[0] * BLOCK_SIZE, (head_block[1] + i) * BLOCK_SIZE))
    return local_snake_head, local_snake_body


def snake_self_colliding(in_snake_head, in_snake_body):
    for snake_part in in_snake_body:
        if snake_part == in_snake_head:
            return True
    return False


def generate_apple():
    return (
        random.randrange(BLOCK_SIZE, DISPLAY_RESOLUTION[0] - BLOCK_SIZE * 2, BLOCK_SIZE),
        random.randrange(BLOCK_SIZE, DISPLAY_RESOLUTION[1] - BLOCK_SIZE * 2, BLOCK_SIZE),
    )


def draw_snake(in_display, in_snake_head, in_snake_body, in_direction):
    for snake_part in in_snake_body:
        pygame.draw.rect(in_display, BLUE_COLOR, (*snake_part, BLOCK_SIZE - BLOCK_GAP_SIZE,
                                                  BLOCK_SIZE - BLOCK_GAP_SIZE))
    rotated_head = pygame.transform.rotate(SNAKE_IMAGE, DIRECTIONS_ANGLES[in_direction])
    in_display.blit(rotated_head, (*in_snake_head, ))


def draw_message(in_display, in_message, in_color, in_y_margin=0, in_font=None, in_font_size=26):
    if in_font is None:
        font = pygame.font.SysFont(in_font, in_font_size)
    else:
        font = pygame.font.Font(in_font, in_font_size)
    text_surface = font.render(in_message, True, in_color)
    surface_rect = text_surface.get_rect(center=(DISPLAY_RESOLUTION[0] // 2,
                                                 DISPLAY_RESOLUTION[1] // 2 + in_y_margin))
    in_display.blit(text_surface, surface_rect)


def draw_score(in_display, in_score, in_color=None, in_font='EVILDEAD.TTF', in_font_size=20):
    if in_color is None:
        in_color = GAME_OVER_COLOR

    font = pygame.font.Font(in_font, in_font_size)
    text_surface = font.render(f'Score {in_score}', True, in_color)
    surface_rect = text_surface.get_rect()
    in_display.blit(text_surface, (DISPLAY_RESOLUTION[0] - (BLOCK_SIZE + 2) - surface_rect[2],
                                   BLOCK_SIZE + 2, surface_rect[2], surface_rect[3]))


# Учитывая бордер!
BLOCKS_WIDTH = 50
BLOCKS_HEIGHT = 40
BLOCK_SIZE = 16
BLOCK_GAP_SIZE = 1

DISPLAY_RESOLUTION = (BLOCKS_WIDTH * BLOCK_SIZE, BLOCKS_HEIGHT * BLOCK_SIZE)
GAME_SPEED = 5

BG_COLOR = (10, 10, 25)
RED_COLOR = (160, 10, 30)
BLUE_COLOR = (5, 20, 140)
GREEN_COLOR = (10, 140, 40)
WHITE_COLOR = (230, 235, 240)
BOUNDS_COLOR = (5, 100, 5)
GAME_OVER_COLOR = (180, 5, 10)

WORLD_SIZE = DISPLAY_RESOLUTION

WORLD_BOUNDS = {
    'min': (BLOCK_SIZE, BLOCK_SIZE),
    'max': (DISPLAY_RESOLUTION[0] - BLOCK_SIZE * 2, DISPLAY_RESOLUTION[1] - BLOCK_SIZE * 2)
}


DIRECTIONS_KEYMAP = {
    pygame.K_LEFT: (-BLOCK_SIZE, 0),
    pygame.K_RIGHT: (BLOCK_SIZE, 0),
    pygame.K_UP: (0, -BLOCK_SIZE),
    pygame.K_DOWN: (0, BLOCK_SIZE),
}

# Углы, на которые должен повернуться спрайт, в зависимости от направления движения.
# Сделано в виде констант для упрощения кода.
DIRECTIONS_ANGLES = {
    (-BLOCK_SIZE, 0): 90,
    (BLOCK_SIZE, 0): 270,
    (0, -BLOCK_SIZE): 0,
    (0, BLOCK_SIZE): 180,
}

GAME_ICON = pygame.image.load('apple_big.png')

SNAKE_IMAGE = pygame.image.load('snake.png')
APPLE_IMAGE = pygame.image.load('apple.png')


if __name__ == '__main__':
    pygame.init()

    # Фикс, грязноватый, но решающий проблему нечётного количества клеток.
    snake_head, snake_body = generate_snake()
    head_direction = (0, -BLOCK_SIZE)
    invalid_direction = get_invalid_direction(snake_head, snake_body[0])

    apple = generate_apple()
    player_score = 0

    clock = pygame.time.Clock()

    game_screen = pygame.display.set_mode(DISPLAY_RESOLUTION, 0, 32)
    pygame.display.set_caption('More snakes to the god of snakes!')
    pygame.display.set_icon(GAME_ICON)

    intro_mode = True
    while intro_mode:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    intro_mode = False
                elif event.key == pygame.K_q:
                    pygame.quit()
                    sys.exit()

        game_screen.fill(BG_COLOR)
        draw_message(game_screen, 'Welcome!', RED_COLOR, -60, 'EVILDEAD.TTF', 50)
        draw_message(game_screen, 'Press Space to start or Q to quit',
                     WHITE_COLOR, 0, 'Blade 2.ttf', 30)
        pygame.display.update()
        clock.tick(GAME_SPEED)

    is_running = True
    is_game_over = False
    while is_running:

        while is_game_over:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        is_game_over = False
                        snake_head, snake_body = generate_snake()
                        head_direction = (0, -BLOCK_SIZE)
                        apple = generate_apple()
                        player_score = 0
                        continue
                    elif event.key == pygame.K_q:
                        pygame.quit()
                        sys.exit()

            game_screen.fill(BG_COLOR)
            draw_message(game_screen, 'Game Over Man!',
                         GAME_OVER_COLOR, -100, 'EVILDEAD.TTF', 60)

            draw_message(game_screen, f'Your score is {player_score}',
                         RED_COLOR, -25, 'EVILDEAD.TTF', 35)

            draw_message(game_screen, 'Press Space to play again or Q to quit',
                         WHITE_COLOR, 42, 'Blade 2.ttf', 30)

            pygame.display.update()
            clock.tick(GAME_SPEED)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                is_running = False
                break  # Чтобы сразу выйти из обработки ивентов.
            elif event.type == pygame.KEYDOWN:
                if event.key in DIRECTIONS_KEYMAP and DIRECTIONS_KEYMAP[event.key] != invalid_direction:
                    head_direction = DIRECTIONS_KEYMAP[event.key]

        new_head = (snake_head[0] + head_direction[0], snake_head[1] + head_direction[1])
        invalid_direction = get_invalid_direction(new_head, snake_head)

        if not in_bounds(new_head, WORLD_BOUNDS['min'], WORLD_BOUNDS['max']):
            is_game_over = True
            continue

        snake_body.appendleft(snake_head)
        snake_head = new_head
        if snake_head == apple:
            apple = generate_apple()
            player_score += 1
        else:
            snake_body.pop()

        if snake_self_colliding(snake_head, snake_body):
            is_game_over = True

        game_screen.fill(BG_COLOR)
        draw_bounds(game_screen, DISPLAY_RESOLUTION, BLOCK_SIZE, BOUNDS_COLOR)
        # Рисуем яблоко.
        game_screen.blit(APPLE_IMAGE, (*apple,))
        draw_snake(game_screen, snake_head, snake_body, head_direction)
        draw_score(game_screen, player_score)

        pygame.display.update()
        clock.tick(GAME_SPEED)

    pygame.quit()
    sys.exit()
