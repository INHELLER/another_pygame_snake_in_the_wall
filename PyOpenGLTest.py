#!/usr/bin/env python3

import OpenGL.GL as GL
import OpenGL.GLU as GLU
import pygame
import sys


def draw_lines(in_vertices, in_edges):
    GL.glBegin(GL.GL_LINES)

    for edge in in_edges:
        # Не вижу смысла создавать второй луп, ради двух элементов.
        GL.glColor3fv((0.7, 0.1, 0.05))
        GL.glVertex3fv(in_vertices[edge[0]])
        GL.glVertex3fv(in_vertices[edge[1]])

    GL.glEnd()


def draw_quads(in_vertices, in_surfaces, in_colors):
    GL.glBegin(GL.GL_QUADS)

    color_index = 0
    for surface in in_surfaces:
        for vertex in surface:
            GL.glColor3fv(in_colors[color_index])
            GL.glVertex3fv(in_vertices[vertex])
            color_index = (color_index + 1) % len(in_colors)

    GL.glEnd()


DEMO_RESOLUTION = (800, 600)
DEMO_ASPECT_RATIO = DEMO_RESOLUTION[0]/DEMO_RESOLUTION[1]
DEMO_FOV = 45.0
DEMO_FPS = 30


if __name__ == '__main__':

    vertices = (
        (1, -1, -1), (1, 1, -1), (-1, 1, -1),
        (-1, -1, -1), (1, -1, 1), (1, 1, 1),
        (-1, -1, 1), (-1, 1, 1),
    )
    edges = (
        (0, 1), (0, 3), (0, 4), (2, 1), (2, 3), (2, 7),
        (6, 3), (6, 4), (6, 7), (5, 1), (5, 4), (5, 7),
    )

    surfaces = (
        (0, 1, 2, 3),
        (3, 2, 7, 6),
        (6, 7, 5, 4),
        (4, 5, 1, 0),
        (1, 5, 7, 2),
        (4, 0, 3, 6),
    )

    colors = (
        (0.8, 0, 0), (0, 0.8, 0), (0, 0, 0.8),
        (0.8, 0.8, 0), (0.8, 0.8, 0.8), (0, 0.8, 0.8),
        (0.8, 0.4, 0), (0, 0.8, 0), (0, 0, 0.8),
        (0.8, 0, 0), (0, 0.8, 0), (0, 0, 0.8),
    )

    pygame.init()
    pygame.display.set_mode(DEMO_RESOLUTION,
                            pygame.DOUBLEBUF | pygame.OPENGL)

    GLU.gluPerspective(DEMO_FOV, DEMO_ASPECT_RATIO, 1, 50.0)
    GL.glTranslatef(0.0, 0.0, -5.0)

    clock = pygame.time.Clock()
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_w:
                    GL.glRotatef(1, 1, 1, 1)
                elif event.key == pygame.K_s:
                    GL.glRotatef(-1, 1, 1, 1)
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 4:
                    GL.glTranslatef(0, 0, 0.2)
                elif event.button == 5:
                    GL.glTranslatef(0, 0, -0.2)

        GL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT)
        GL.glTranslatef(2, 0, 0)
        draw_lines(vertices, edges)
        GL.glTranslatef(-2, 0, 0)
        draw_quads(vertices, surfaces, colors)
        pygame.display.flip()

        clock.tick(DEMO_FPS)
